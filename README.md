# Agilent .mi Python loader

Experimental loader for Agilent .mi files from Keysight AFM/STM in Python.

There are three example .mi files with garbage data, just to test the loader.
Usage is illustrated in parse_test.ipynb.
Good luck :)